
/*
    cciview.c

    CCI file viewer
    BDS C compiler utility

    (C) Thierry Peycru, 2021
*/

#include "stdint.h"
#include <stdio.h>
#include <string.h>

#define CCIEXT ".cci"
#define TXTEXT ".txt"

//
// grab and return a word from file
//
int rdwd(FILE *fd)
{
    int lsb, msb;
    lsb = fgetc(fd);
    msb = fgetc(fd);
    return (lsb + 256*msb);
}

//
// output symbol entry from current file pointer
//
int symentry(FILE *fd, FILE *fo)
{
    uint16_t byte8, byte9;
    char *what[4] = { "VAR", "FUN", "STT", "REF" };
    char *type[8] = { "char  ", "int   ", "uns   ", "long  ", "float ", "double", "struct", "void  " };

    byte8 = fgetc(fd);
    byte9 = fgetc(fd);

    if ( (byte8 & 0X000F) == 0X000E )
    {
        fprintf(fo, "INI %2Xh    ", byte9);
        fprintf(fo, "w1  :%4Xh ", rdwd(fd));
        fprintf(fo, "w2  :%4Xh ", rdwd(fd));
        fprintf(fo, "w3  :%4Xh ", rdwd(fd));
    }
    else 
    {
        fprintf(fo, "%s %s ", what[ byte8 & 0X0003 ], type[ (byte8 & 0X0070) >> 4 ] );
        fprintf(fo, "adrs:%4Xh ", rdwd(fd)); // ADRS
        fprintf(fo, "size:%4Xh ", rdwd(fd)); // SIZE
        fprintf(fo, "dims:%4Xh ", rdwd(fd)); // DIMSZ
        fprintf(fo, "lind:%u clev:%u ", (byte9 & 0X00C0) >> 6, byte9 & 0X003F);
        if (byte8 & 0X0080) fprintf(fo, "ptfn ");
        if (byte8 & 0X0004) fprintf(fo, "frml ");
        if (byte8 & 0X0008) fprintf(fo, "stel ");
    }
    fprintf(fo, "\n");

    return 0;
}

//
// cciview filename (without extension)
//
int main( int argc, char *argv[] )
{
    uint16_t extsz, symsz, fnmsz, symcnt, symnb, fnmcnt, fnmcnn;
    uint8_t fnmch;
    char finp[20];
    char fout[20];
    char fnmstr[10];
    FILE *fd;
    FILE *fo;

    char *token[128] = {    "char","int","unsigned","","","","","",
                            "","","","struct","union","goto","return","sizeof",
                            "break","continue","if","else","for","do","while","switch",
                            "case","default","","{","}","main","register","short",
                            "+=","-=","*=","/=","%%=",">>=","<<=","&=",
                            "^=","|=","==","!=","&&","||","<=",">=",
                            "<<",">>","++","--","->","-","*","/",
                            "%%",">","<","&","^","|","=","!",
                            "?","","(",")","+",".",";",",",
                            "[","]",":","~","","","","",
                            "","","","","","","","",
                            "","","","","","","","",
                            "","","","","","","","",
                            "","","","","","","","",
                            "","","","","","module","modend\n","\n",
                            "const","var","label","","ref","string","\nswitch table","" };

    if (argc != 2)
    {
        puts("cciview filename");
        return 1;
    }

    strcpy(finp, argv[1]);
    strcat(finp, CCIEXT);
    strcpy(fout, argv[1]);
    strcat(fout, TXTEXT);
    if ( (fd = fopen(finp, "r")) == 0) return 1;
    if ( (fo = fopen(fout, "w")) == 0) return 1;

    extsz  = rdwd(fd);
    symsz  = rdwd(fd);
    fnmsz  = rdwd(fd);
    symnb  = symsz >> 3; // div /8
    symcnt = 0;
    fnmcnt = 0;

    // header
 
    fprintf( fo, "filename             : %s\n", finp);
    fprintf( fo, "External area size   : %4Xh\n", extsz);
    fprintf( fo, "Symbol table size    : %4Xh (%d entries)\n", symsz, symnb);
    fprintf( fo, "Func name table size : %4Xh\n", fnmsz);

    // symbols

    fprintf( fo, "\nSymbol table\n");
    for ( symcnt = 0; symcnt < symsz; symcnt += 8 )
    {
        fprintf( fo, "%4d : ", symcnt >> 3);
        symentry( fd, fo);
    }

    // functions

    fprintf( fo, "\nFunction names\n");
    while ( fnmcnt < fnmsz )
    {
        fprintf( fo, "%4d : ", rdwd(fd) );
        fnmcnt += 2;
        fnmcnn = 0;

        do
        {
            fnmch = fgetc(fd);
            fnmstr[fnmcnn] = fnmch & 127;
            fnmcnn++;
        }
        while ( !( fnmch & 128 ) );

        fnmstr[fnmcnn] = 0; // append null terminator
        fnmcnt += fnmcnn;
        fprintf( fo, "%s\n", fnmstr );
    }
    
    // tokenized code

    uint16_t tk, ch, nc, cs, csx, csc;
    int n;
    
    fprintf( fo, "\nTokenized code\n");
    while ( (tk = fgetc(fd)) )
    {
        n  = 0;
        if  ( tk & 0X0080 )
        {
            fprintf( fo, "%s ", token[tk & 0X007F] );        
            switch( tk )
            {
                case 0X00F5: // module begin
                {
                    while( ++n != 17 )
                    {
                        ch = fgetc(fd);
                        if (ch >= ' ' ) fputc( ch, fo );
                    }
                    fputc( '\n', fo );
                    n = 0;
                    break;
                }
                case 0X00F8: // constant
                case 0X00F9: // var
                case 0X00FA: // label
                case 0X00FC: // label reference
                case 0X00FD: // string
                {
                    fprintf( fo, "%d ", rdwd(fd) );
                    break;
                }
                case 0X00FE: // switch table
                {
                    nc = fgetc(fd);
                    fprintf( fo, "[%d]\n", nc);
                    for ( cs = 0; cs < nc; cs+=1 )
                    {
                        csx = rdwd(fd);
                        csc = (csx < 32) ? 32 : csx ;
                        fprintf(fo, "%5d %c : ", csx, csc );
                        fprintf(fo, "ref %d\n",    rdwd(fd) );
                    }
                    fprintf(fo, "default : ref %d\n", rdwd(fd) );
                    break;
                }
            }
        }
        else if (tk >= ' ' )
        {
            fputc( tk, fo );
        }
    }

    // strings

    uint16_t lsbs, msbs;

    fprintf( fo, "\nStrings\n");

    while ( 1 )
    {
        if ( !feof(fd) ) lsbs = fgetc(fd);
        else break;
        if ( !feof(fd) ) msbs = fgetc(fd);
        else break;        
        if ( !feof(fd) ) ch = fgetc(fd);
        else break;

        fprintf( fo, "%4d: ", msbs*256 + lsbs );
        while( ch )
        {
            if ( (tk = fgetc(fd)) > 31 ) fputc( tk, fo );
            ch--;
        }
        fputc( '\n', fo );
    }

    // the end

    fclose(fd);
    fclose(fo);

    fprintf(stdout, "BDS C compiler - CCI intermediate file viewer\nGenerated %s\n",fout);
    return 0;
}
