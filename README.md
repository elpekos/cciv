# CCI Viewer
(C) Thierry Peycru, 2021

BDS C CCI intermediate file viewer

Generates a text file with:
- Global data
- Detailed symbol table
- Function name table
- Tokenized code
- String table

Usage : cciv filename (without .cci extension)
