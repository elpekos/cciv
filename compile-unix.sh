#!/bin/bash

# *************************************************************************************
#
# CCI file viewer utility compilation script
#
# *************************************************************************************


command -v gcc >/dev/null 2>&1 || { echo "gcc not found on system" >&2; exit 1; }
gcc cciv.c -o cciv
chmod +x ./cciv
